package com.example.demo1811.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(Property.class)
public abstract class Property_ {

	public static volatile SingularAttribute<Property, String> skin;
	public static volatile SingularAttribute<Property, Integer> hp;
	public static volatile SingularAttribute<Property, Integer> id;
	public static volatile SingularAttribute<Property, String> weaponName;
	public static volatile SingularAttribute<Property, Integer> weapons;

	public static final String SKIN = "skin";
	public static final String HP = "hp";
	public static final String ID = "id";
	public static final String WEAPON_NAME = "weaponName";
	public static final String WEAPONS = "weapons";

}

