package com.example.demo1811.entity;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(UserTest.class)
public abstract class UserTest_ {

	public static volatile SingularAttribute<UserTest, String> password;
	public static volatile SingularAttribute<UserTest, String> address;
	public static volatile SingularAttribute<UserTest, Boolean> isDeleted;
	public static volatile SingularAttribute<UserTest, String> sex;
	public static volatile SingularAttribute<UserTest, Property> property;
	public static volatile SingularAttribute<UserTest, Integer> id;
	public static volatile SingularAttribute<UserTest, String> userName;
	public static volatile SingularAttribute<UserTest, Integer> age;

	public static final String PASSWORD = "password";
	public static final String ADDRESS = "address";
	public static final String IS_DELETED = "isDeleted";
	public static final String SEX = "sex";
	public static final String PROPERTY = "property";
	public static final String ID = "id";
	public static final String USER_NAME = "userName";
	public static final String AGE = "age";

}

