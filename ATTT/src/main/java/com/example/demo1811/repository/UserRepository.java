package com.example.demo1811.repository;

import com.example.demo1811.entity.UserTest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserTest, Integer>, JpaSpecificationExecutor<UserTest> {

    UserTest findUserByUserName(@Param("name") String name);

}
