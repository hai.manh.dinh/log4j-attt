package com.example.demo1811;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan(basePackages = {"com.example.demo1811.entity"})
@EnableJpaRepositories(basePackages = {"com.example.demo1811.repository"})
@ConfigurationPropertiesScan
public class Demo1811Application {

    public static void main(String[] args) {
        SpringApplication.run(Demo1811Application.class, args);
    }

}
