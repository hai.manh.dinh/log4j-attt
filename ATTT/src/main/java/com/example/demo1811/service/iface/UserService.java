package com.example.demo1811.service.iface;

import com.example.demo1811.dto.UserDto;

import java.util.List;

public interface UserService {
    List<UserDto> getAllUsers(Integer page, Integer limit, String sort);

    Integer addUser(UserDto dto);

    Integer changeUserInfo(UserDto dto, Integer id);

    UserDto getUserByName(String name);

    Boolean deleteUser(Integer id);

    UserDto getUserById(Integer id);
}
