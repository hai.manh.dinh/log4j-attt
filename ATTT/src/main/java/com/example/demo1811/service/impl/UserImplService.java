package com.example.demo1811.service.impl;

import com.example.demo1811.dto.UserDto;
import com.example.demo1811.entity.UserTest;
import com.example.demo1811.filter.UserFilter;
import com.example.demo1811.mapper.UserMapper;
import com.example.demo1811.repository.UserRepository;
import com.example.demo1811.service.iface.UserService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.*;
import java.util.stream.Collectors;


@Service
public class UserImplService implements UserService {

    private final UserMapper userMapper;

    private final UserFilter userFilter;

    private final UserRepository userRepository;


    public UserImplService(UserMapper userMapper, UserFilter userFilter, UserRepository repository) {
        this.userMapper = userMapper;
        this.userFilter = userFilter;
        this.userRepository = repository;
    }

    @Override
    public List<UserDto> getAllUsers(Integer page, Integer limit, String sort) {
        Map<String, String> map = new HashMap();
        map.put("id", "desc");
        List<UserDto> tmp = new ArrayList<>();
        Page<UserTest> result = userRepository.findAll(userFilter.filter(0, map), PageRequest.of(page - 1, limit));
        if (!result.isEmpty()) {
            tmp = result.getContent().stream().map(userMapper::toUserDto).collect(Collectors.toList());
        }
        return tmp;
    }

    @Override
    public Integer addUser(UserDto dto) {
        UserTest tmp = userRepository.findUserByUserName(dto.getUsername());
        if (tmp != null) return null;
        tmp = new UserTest();
        tmp.setUserName(dto.getUsername());
        tmp.setPassword(dto.getPassword());
        tmp.setAge(dto.getAge());
        tmp.setSex(dto.getSex());
        tmp.setAddress(dto.getAddress());
        tmp.setIsDeleted(false);
        userRepository.save(tmp);
        return tmp.getId();
    }

    @Override
    public Integer changeUserInfo(UserDto dto, Integer id) {
        Optional<UserTest> tmp = userRepository.findById(id);
        if (tmp == null) return null;
        UserTest user = tmp.get();
        user.setUserName(dto.getUsername());
        user.setPassword(dto.getPassword());
        user.setAge(dto.getAge());
        user.setSex(dto.getSex());
        user.setAddress(dto.getAddress());
        userRepository.save(user);
        return user.getId();
    }

    @Override
    public UserDto getUserByName(String name) {
        return userMapper.toUserDto(userRepository.findUserByUserName(name));
    }

    @Override
    public Boolean deleteUser(Integer id) {
        Optional<UserTest> tmp = userRepository.findById(id);
        if (tmp.isPresent()) {
            UserTest tmp2 = tmp.get();
            tmp2.setIsDeleted(true);
            userRepository.save(tmp2);
            return true;
        }
        return false;
    }

    @Override
    public UserDto getUserById(Integer id) {
        Optional<UserTest> tmp = userRepository.findById(id);
        if(tmp.isEmpty()) return null;
        UserTest user = tmp.get();
        return userMapper.toUserDto(user);
    }
}
