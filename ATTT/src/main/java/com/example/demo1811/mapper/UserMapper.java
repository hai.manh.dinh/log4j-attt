package com.example.demo1811.mapper;

import com.example.demo1811.dto.UserDto;
import com.example.demo1811.entity.UserTest;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class UserMapper {

    public abstract UserDto toUserDto(UserTest entity);
}
