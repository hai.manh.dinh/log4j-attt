package com.example.demo1811.mapper;

import com.example.demo1811.dto.UserDto;
import com.example.demo1811.entity.UserTest;
import org.springframework.stereotype.Component;


@Component
public class UserMapperImpl extends UserMapper {
    @Override
    public UserDto toUserDto(UserTest entity) {
        if (entity == null) return null;

        UserDto userDto = new UserDto();
        userDto.setUsername(entity.getUserName());
        userDto.setAge(entity.getAge());
        userDto.setAddress(entity.getAddress());
        userDto.setPassword(entity.getPassword());
        userDto.setSex(entity.getSex());
        return userDto;
    }
}
