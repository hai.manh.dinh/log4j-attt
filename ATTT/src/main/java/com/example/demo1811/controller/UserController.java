package com.example.demo1811.controller;


import com.example.demo1811.config.BaseMethodResponse;
import com.example.demo1811.config.Constants;
import com.example.demo1811.config.GetMethodResponse;
import com.example.demo1811.config.PostMethodResponse;
import com.example.demo1811.dto.UserDto;
import com.example.demo1811.service.iface.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
public class UserController {
    private final UserService services;

    public UserController(UserService services) {
        this.services = services;
    }

    @GetMapping("/users")
    public ResponseEntity<List<UserDto>> getAllUsers(@RequestParam(value = "page", required = false, defaultValue = "1") Integer page,
                                                     @RequestParam(value = "limit", required = false, defaultValue = "3") Integer limit,
                                                     @RequestParam(value = "sort", required = false, defaultValue = "") String sort) {
        List<UserDto> result = services.getAllUsers(page, limit, sort);
        return new ResponseEntity(
                GetMethodResponse.builder().status(true).message(Constants.SUCCESS_MSG)
                        .data(result)
                        .errorCode(HttpStatus.OK.name().toLowerCase()).httpCode(HttpStatus.OK.value())
                        .build(), HttpStatus.OK);
    }

//    @GetMapping("/user/{name}")
//    public ResponseEntity<UserDto> getUserByName(@PathVariable(value = "name") String name) {
//        UserDto result = services.getUserByName(name);
//        if(result == null) {
//            return new ResponseEntity(
//                    PostMethodResponse.builder().status(false)
//                            .message(Constants.ERROR_MSG).errorCode(HttpStatus.NOT_FOUND.name().toLowerCase())
//                            .httpCode(HttpStatus.NOT_FOUND.value()).build()
//                    , HttpStatus.NOT_FOUND);
//        }
//        return new ResponseEntity(
//                GetMethodResponse.builder().status(true).message(Constants.SUCCESS_MSG)
//                        .data(result)
//                        .errorCode(HttpStatus.OK.name().toLowerCase()).httpCode(HttpStatus.OK.value())
//                        .build(), HttpStatus.OK);
//    }

    @GetMapping("/user/{id}")
    public ResponseEntity<UserDto> getUserById(@PathVariable(value = "id") Integer id) {
        UserDto result = services.getUserById(id);
        if(result == null) {
            return new ResponseEntity(
                    PostMethodResponse.builder().status(false)
                            .message(Constants.ERROR_MSG).errorCode(HttpStatus.NOT_FOUND.name().toLowerCase())
                            .httpCode(HttpStatus.NOT_FOUND.value()).build()
                    , HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(
                GetMethodResponse.builder().status(true).message(Constants.SUCCESS_MSG)
                        .data(result)
                        .errorCode(HttpStatus.OK.name().toLowerCase()).httpCode(HttpStatus.OK.value())
                        .build(), HttpStatus.OK);
    }

    @PostMapping("/user")
    public ResponseEntity<?> addUser(@RequestBody UserDto dto) {
        Integer result = services.addUser(dto);
        if(result == null) {
            return new ResponseEntity<>(
                    PostMethodResponse.builder().status(false).id(result)
                            .message(Constants.ERROR_MSG).errorCode(HttpStatus.CONFLICT.name().toLowerCase())
                            .httpCode(HttpStatus.CONFLICT.value()).build()
                    , HttpStatus.CONFLICT);
        }
        return new ResponseEntity<>(
                PostMethodResponse.builder().status(true).id(result)
                        .message(Constants.SUCCESS_MSG).errorCode(HttpStatus.OK.name().toLowerCase())
                        .httpCode(HttpStatus.OK.value()).build()
                , HttpStatus.OK);
    }

    @PutMapping("/user/{id}")
    public ResponseEntity<Integer> changeUserInfo(@RequestBody @Valid UserDto dto, @PathVariable(value = "id") Integer id) {
        Integer result = services.changeUserInfo(dto, id);
        if(result == null) {
            return new ResponseEntity(
                    PostMethodResponse.builder().status(false).id(result)
                            .message(Constants.ERROR_MSG).errorCode(HttpStatus.NOT_FOUND.name().toLowerCase())
                            .httpCode(HttpStatus.NOT_FOUND.value()).build()
                    , HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(
                PostMethodResponse.builder().status(true).id(result)
                        .message(Constants.SUCCESS_MSG).errorCode(HttpStatus.OK.name().toLowerCase())
                        .httpCode(HttpStatus.OK.value()).build()
                , HttpStatus.OK);
    }

    @DeleteMapping("/user/{id}")
    public ResponseEntity<Boolean> deleteUser(@PathVariable(value = "id") Integer id) {
        Boolean result = services.deleteUser(id);
        if(result == null) {
            return new ResponseEntity(
                    PostMethodResponse.builder().status(false).id(result)
                            .message(Constants.ERROR_MSG).errorCode(HttpStatus.NOT_FOUND.name().toLowerCase())
                            .httpCode(HttpStatus.NOT_FOUND.value()).build()
                    , HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity(
                BaseMethodResponse.builder().status(true).message(Constants.SUCCESS_MSG)
                        .httpCode(HttpStatus.OK.value()).build()
                , HttpStatus.OK);
    }
}
