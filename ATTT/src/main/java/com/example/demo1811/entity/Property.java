package com.example.demo1811.entity;

import javax.persistence.*;

@Entity
@Table(name = "property")
public class Property {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Integer weapons;
    private String weaponName;
    private String skin;
    private Integer hp;
}
